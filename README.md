[![Pablo Urrea Badge](https://pablourrea.github.io/sites/default/files/badges/pablourrea.svg)](https://pablourrea.github.io)


# Tome Add Paths

## Introduction

The [Tome Add Paths](https://www.drupal.org/project/tome_add_paths/) module allows you to force the export of web paths, folder paths (exporting all their contents) and specific files.


## Requirements

This module requires "Tome Sync" module from "Tome" ecosystem.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the Tome Add Paths module at Administration > Extend.
2. Configure the module at Administration > Configuration > Tome Static > Configure Tome Paths.


## Current Active Maintainer

- Pablo Urrea - [enchufe](https://www.drupal.org/u/enchufe)


## Bug Reports & New Features

Please report bugs to the [Drupal.org issue queue](https://www.drupal.org/project/issues/tome_add_paths).

Note that the maintainers are not actively supporting development of new
features. However, contributions to add features are welcome provided they are
narrow in scope, well written, and well documented.
