<?php

namespace Drupal\Tests\tome_add_paths\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\tome_static\Event\CollectPathsEvent;
use Drupal\tome_static\Event\TomeStaticEvents;

/**
 * Tests the functionality of Tome Add Paths.
 *
 * @coversDefaultClass \Drupal\tome_add_paths\EventSubscriber\AddPathsEventSubscriber
 * @group tome_add_paths
 */
class TomeAddPathsTest extends BrowserTestBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'tome_add_paths',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configFactory = \Drupal::service('config.factory');
    $this->eventDispatcher = \Drupal::service('event_dispatcher');

    $this->drupalLogin($this->createUser(['use tome static']));
    $this->drupalCreateContentType(['type' => 'article']);
    $this->drupalCreateNode(['type' => 'article', 'title' => 'My First Article', 'path' => '/my-first-path']);
    $this->drupalCreateNode(['type' => 'article', 'title' => 'My Second Article', 'path' => '/my-second-path']);

    $this->drupalGet('/admin/config/tome/config');
    $this->SubmitForm([
      'paths' => '/my-first-path\r\n/my-second-path',
    ], 'Save configuration');
  }

  /**
   * Tests the Tome Add Paths admin form.
   */
  public function testAddPathsForm() {
    $config = $this->configFactory->get('tome_add_paths.config')->get('paths');
    $paths = explode('\r\n', str_replace("\r\n", '\r\n', $config));
    $this->assertSame($paths, ['/my-first-path', '/my-second-path']);
  }

  /**
   * @covers \Drupal\tome_add_paths\EventSubscriber\AddPathsEventSubscriber::addPaths
   */
  public function testAddPaths() {
    $this->eventDispatcher->addListener(TomeStaticEvents::COLLECT_PATHS, [$this, 'addPaths']);
    $event = new CollectPathsEvent([]);
    $this->eventDispatcher->dispatch($event, TomeStaticEvents::COLLECT_PATHS);
    $paths = $event->getPaths();
    $this->assertContains('/my-first-path', $paths);
    $this->assertContains('/my-second-path', $paths);
  }

  /**
   * Emulates the addPaths() listener.
   *
   * @param \Drupal\tome_static\Event\CollectPathsEvent $event
   *   The collect paths event.
   */
  public function addPaths(CollectPathsEvent $event) {
    $config = $this->configFactory->get('tome_add_paths.config')->get('paths');
    $paths = explode('\r\n', str_replace("\r\n", '\r\n', $config));
    $event->addPaths($paths);
  }

}
