<?php

namespace Drupal\tome_add_paths\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Config form for Tome Add Paths.
 *
 * @package Drupal\tome_add_paths\Form
 */
class TomeAddPathsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tome_add_paths.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tome_add_paths_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tome_add_paths.config');

    $form['paths'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Paths'),
      '#default_value' => $config->get('paths'),
      '#description' => $this->t('Supports web paths, directory paths and file paths.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('tome_add_paths.config')
      ->set('paths', $form_state->getValue('paths'))
      ->save();
  }

}
