<?php

namespace Drupal\tome_add_paths\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\Exception\NotRegularDirectoryException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\tome_base\PathTrait;
use Drupal\tome_static\Event\CollectPathsEvent;
use Drupal\tome_static\Event\TomeStaticEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Add paths to the list of paths to export.
 */
class AddPathsEventSubscriber implements EventSubscriberInterface {

  use PathTrait;

  use StringTranslationTrait;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Tome Add Paths configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $tomeAddPathsConfig;

  /**
   * Constructs the AddPathsEventSubscriber object.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(MessengerInterface $messenger, FileSystemInterface $file_system, ConfigFactoryInterface $config_factory) {
    $this->messenger = $messenger;
    $this->fileSystem = $file_system;
    $this->configFactory = $config_factory;
    $this->tomeAddPathsConfig = $config_factory->get('tome_add_paths.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[TomeStaticEvents::COLLECT_PATHS][] = ['addPaths'];
    return $events;
  }

  /**
   * Reacts to a collect paths event.
   *
   * @param \Drupal\tome_static\Event\CollectPathsEvent $event
   *   The collect paths event.
   */
  public function addPaths(CollectPathsEvent $event) {
    $paths = $this->tomeAddPathsConfig->get('paths');
    if (empty($paths)) {
      return;
    }

    $event_paths = [];
    $paths = explode('\r\n', str_replace("\r\n", '\r\n', $paths));
    foreach ($paths as $path) {
      $url = Url::fromUri('internal:' . $path);

      // Check if this path has a Drupal route.
      if ($url->isRouted()) {
        $event_paths[] = $path;
        continue;
      }

      // Check if this path is a directory.
      if (is_dir('.' . $path)) {
        try {
          if (substr($path, 0, 1) !== '.') {
            $path = '.' . $path;
          }

          $sub_paths = $this->fileSystem->scanDirectory($path, '/.*/');
          foreach ($sub_paths as $sub_path => $object) {
            if (substr($sub_path, 0, 1) === '.') {
              $sub_path = substr($sub_path, 1);
            }
            $event_paths[] = $sub_path;
          }
        }
        catch (NotRegularDirectoryException | \Exception $e) {
          $this->messenger->addError($this->t('An error occurred adding the route @route: @error.', [
            '@route' => $path,
            '@error' => $e->getMessage(),
          ]));
        }
        continue;
      }

      // Check if this path is a file.
      if (is_file('.' . $path)) {
        $event_paths[] = $path;
        continue;
      }

      $this->messenger->addWarning($this->t('An error occurred adding the route @route: It is not a valid route.', ['@route' => $path]));
    }

    $event->addPaths($event_paths);
  }

}
